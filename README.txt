Luis Lain


1 Introduction
==============

  - Subscription system
    - Public service, isolated, implement security
    - Subscription service, implement bussiness logic and persistence
    - Email service, notify subscriptions
  - API Documentation
    - Swagger
  - UI example
    [https://www.adidas.co.uk/on/demandware.store/Sites-adidas-GB-Site/en_GB/Newsletter-Subscribe]
  - Bonus
    - CI/CD proposal
    - Config files to deploy into a kubernetes cluster


2 Quick start
=============

2.1 Prerequisites
~~~~~~~~~~~~~~~~~

  [GIT 2.25.1], [JDK 11], [MAVEN 3.8.1], [Docker 19.03.8], [Docker
  compose 1.28.5],


[GIT 2.25.1] https://git-scm.com/downloads

[JDK 11]
https://www.oracle.com/java/technologies/javase-jdk11-downloads.html

[MAVEN 3.8.1]
https://ftp.cixug.es/apache/maven/maven-3/3.8.1/binaries/apache-maven-3.8.1-bin.zip

[Docker 19.03.8] https://docs.docker.com/engine/install/

[Docker compose 1.28.5] https://github.com/docker/compose/releases


2.2 Installation
~~~~~~~~~~~~~~~~

  - Clone this repository with the submodules
    ,----
    | git clone --recurse-submodules https://gitlab.com/lll-tools/spring/lll-subscription-system.git
    | cd lll-subscription-system
    `----
  - Build project
    This script runs `mvn package' on each submodule. The script starts
    a docker MySQL
    instance that is required to build the Service *servicejpamysql*.
    ,----
    | ./bin/maven-build.sh
    `----
  - Run containers with the services:
    - *servicemysqldb*, MySQL 8.0.23, exposes 3306
    - *serviceproxy*, NGINX 1.19, exposes 8383
    - *servicepublicrest*, exposes 8080, implements public API
    - *servicejpamysql*, exposes 8181, implements bussiness logic and
       persistence
    - *servicejpa*, exposes 8282, alternative implementation
    ,----
    | docker-compose up --build -d
    `----
    In the file `docker-compose.yml' are two custom networks defined:
    *frontend* and
    *backend*. Only the Service *serviceproxy* has visibility in both of
     them.
    ,----
    | networks:
    |   frontend:
    |   backend:
    `----
  - Check application
    ,----
    | ./bin/curl-test-app.sh
    `----
  - Create example data
    ,----
    | ./bin/curl-load-data.sh
    | ./bin/curl-subscription-add.sh "Name" "email@domain.com"
    `----


3 Implementation
================

3.1 Tools
~~~~~~~~~

  - [GNU Emacs 26.3], [Visual Studio Code 1.55.1], [Spring Boot 2.4.4],


[GNU Emacs 26.3] https://www.gnu.org/software/emacs/

[Visual Studio Code 1.55.1] https://code.visualstudio.com/download

[Spring Boot 2.4.4] https://spring.io/projects/spring-boot


3.2 Using REST and Mysql (8080)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  The Service *servicepublicrest* receives REQUEST from the UI using a
  `@RestController'
  and connects with the Service *serviceproxy* in the frontend network
  using a
  `@RestTemplate'. The Service *serviceproxy* connects to the Service
  *servicejpamysql* in
  the backend network with access to the Database using the
  `@Controller' and the
  `@ResponseBody' annotations. The Service *servicejpamysql* access the
  database
  *servicemysqldb* with the JPA SubscriptionRepository injected in the
   Controller using the
  `@Autowired' annotation.
  [file:doc/subscription_small.png]


3.2.1 servicejpamysql
---------------------

  - Use Spring Initializr to create a new Maven project with the
    following dependencies:
    - Spring Web, Spring Data JPA, MySQL Driver
    - 2.4.4, Java, com.luislain.spring.subscription, servicejpamysql,
      Jar, 11
    Key entries in `pom.xml'
    ,----
    | <dependency>
    | 	<groupId>org.springframework.boot</groupId>
    | 	<artifactId>spring-boot-starter-data-jpa</artifactId>
    | </dependency>
    | <dependency>
    | 	<groupId>org.springframework.boot</groupId>
    | 	<artifactId>spring-boot-starter-web</artifactId>
    | </dependency>
    `----
  - Create Entity class
    Use the `@Entity' annotation to map the class with a table of the
    JPA persistence database.
    ,----
    | @Entity
    | public class Subscription {
    | 
    |     @Id
    |     @GeneratedValue(strategy = GenerationType.AUTO)
    |     private long id;
    | 
    |     private String email;
    |     private String firstName; // Optional
    |     private String gender; // Optional
    |     private String dateofbirthString;
    |     private Boolean consentString;
    | 
    `----
  - Create JPA SubscriptionRepository interface
    The SubscriptionRepository class is automatically created by Spring
    Boot and injected
    int the controller with the `@Autowired' annotation.
    com.luislain.spring.subscription.servicejpamysql.SubscriptionRepository
    ,----
    | public interface SubscriptionRepository extends CrudRepository<Subscription, Long> {
    | }
    `----
  - Create Controller class
    The Controller class access the database with an instance of the
    `SubscriptionRepository' injected with the `@Autowired' annotation.
    `com.luislain.spring.subscription.servicejpamysql.SubscriptionController.java'
    ,----
    | @Controller
    | @RequestMapping(path = "/api")
    | public class SubscriptionController {
    | 
    |     @Autowired
    |     private SubscriptionRepository subscriptionRepository;
    | 
    `----
    - endpoint `/api/all', list all subscriptions
      ,----
      | @GetMapping(path = "/all")
      | public @ResponseBody Iterable<Subscription> getAllSubscriptions() {
      |     // This returns a JSON or XML
      |     return subscriptionRepository.findAll();
      | }
      `----
      Test the endpoint
      ,----
      | ./bin/curl-jpamysql-all.sh
      `----

      ,----
      | [{"email":"user10@domain.com","firstName":"User10","gender":"Female","dateofbirthString":"1999/01/20","consentString":true},{"email":"user11@domain.com","firstName":"User11","gender":"Male","dateofbirthString":"1999/01/21","consentString":true},{"email":"user12@domain.com","firstName":"User12","gender":"Female","dateofbirthString":"1999/01/22","consentString":true},{"email":"user13@domain.com","firstName":"User13","gender":"Male","dateofbirthString":"1999/01/23","consentString":true},{"email":"user14@domain.com","firstName":"User14","gender":"Female","dateofbirthString":"1999/01/24","consentString":true},{"email":"user15@domain.com","firstName":"User15","gender":"Male","dateofbirthString":"1999/01/25","consentString":true}]
      `----

    - endpoint `/api/add', add new subscription
      ,----
      | @GetMapping(path = "/add")
      | public @ResponseBody String addSubscription(@RequestParam String email, @RequestParam String name,
      | 					    @RequestParam String gender, @RequestParam String dateofbirthString, @RequestParam String consentString) {
      |     Subscription n = new Subscription();
      |     n.setFirstName(name);
      |     n.setEmail(email);
      |     n.setGender(gender);
      |     n.setDateofbirthString(dateofbirthString);
      |     if (consentString.startsWith("T")) {
      | 	n.setConsentString(true);
      |     } else {
      | 	n.setConsentString(false);
      |     }
      |     subscriptionRepository.save(n);
      |     return "Saved";
      | }
      `----
      Test the endpoint
      ,----
      | ./bin/curl-jpamysql-add.sh "Name" "name@domain.com"
      `----

      ,----
      | Saved
      `----

    - endpoint `/api/details', show subscription info
      ,----
      | @GetMapping(path = "/details")
      | public @ResponseBody Optional<Subscription> addNewSubscription(@RequestParam Long id) {
      |     return subscriptionRepository.findById(id);
      | }
      `----
      Test the endpoint
      ,----
      | ./bin/curl-jpamysql-details.sh
      `----

    - endpoint `/api/cancel', delete subscription in database
      ,----
      | @GetMapping(path = "/cancel")
      | public @ResponseBody String cancelSubscription(@RequestParam Long id) {
      |     subscriptionRepository.deleteById(id);
      |     return "Canceled";
      | }
      `----
      Test the endpoint
      ,----
      | ./bin/curl-jpamysql-cancel.sh
      `----

  - Use a CommandLineRunner `@Bean' to load example data in the databse
    at the
    Applicaction startup.
    `com.luislain.spring.subscription.servicejpamysql.SubscriptionExampleData.java'
    ,----
    | @Configuration
    | public class SubscriptionExampleData {
    |     private static final Logger log = LoggerFactory.getLogger(SubscriptionExampleData.class);
    | 
    |     @Bean
    |     CommandLineRunner initDatabase(SubscriptionRepository repository) {
    | 
    | 	return args -> {
    | 	    log.info("Preloading "
    | 		     + repository.save(new Subscription("user10@domain.com", "User10", "Female", "1999/01/20", true)));
    | 
    `----

  - Create SubscriptionNotFoundException and SubscriptionNotFoundAdvice
    classes to manage
    Exceptions in the Controller.
    `com.luislain.spring.subscription.servicejpamysql.SubscriptionNotFoundAdvice.java'
    ,----
    | @ControllerAdvice
    | public class SubscriptionNotFoundAdvice {
    |     @ResponseBody
    |     @ExceptionHandler(SubscriptionNotFoundException.class)
    |     @ResponseStatus(HttpStatus.NOT_FOUND)
    |     String employeeNotFoundHandler(SubscriptionNotFoundException ex) {
    | 	return ex.getMessage();
    |     }
    | }
    `----


3.2.2 servicepublicrest
-----------------------

  - Use Spring Initializr to create a new Maven project with the
    following dependencies:
    - Spring Web
  - Inject the `@Bean' RestTemplate in Application class
    `com/luislain/spring/subscription/servicepublicrest/ServicepublicrestApplication.java'
    ,----
    | @SpringBootApplication
    | public class ServicepublicrestApplication {
    | 
    |     public static void main(String[] args) {
    | 
    | 	SpringApplication.run(ServicepublicrestApplication.class, args);
    |     }
    | 
    |     @Bean
    |     public RestTemplate restTemplate(RestTemplateBuilder builder) {
    | 	return builder.build();
    |     }
    `----
  - Create the Controller class and instantiate the RestTemplate with
    `@Autowired'
    annotation
    The Controller class receives the requests from the UI and uses the
    RestTemplate to
    consume (through the *serviceproxy*) the api provided in the backend
    network for the
    *servicejpamysql*.
    `com/luislain/spring/subscription/servicepublicrest/PublicrestController.java'
    ,----
    | @RestController
    | @RequestMapping(path = "/api")
    | public class PublicrestController {
    | 
    |     private String subscriptionServiceURL = "http://serviceproxy:8080/";
    | 
    |     @Autowired
    |     private RestTemplate restTemplate;
    | 
    |     @GetMapping("/subscriptions")
    |     public Subscription[] getAllSubscriptions() {
    | 	Subscription[] subscriptions = restTemplate.getForObject(
    | 	  subscriptionServiceURL + "api/all", Subscription[].class);
    | 	return subscriptions;
    |     }
    | 
    `----
    The Service *servicepublicrest* can access to the host
    [http://serviceproxy:8080/]
    because both containers are named properly and both connected to the
    network
    *frontend* defined in the `docker-compose.yml' file.
    - endpoint `api/subscriptions', list all subscriptions
      ,----
      | @GetMapping("/subscriptions")
      | public Subscription[] getAllSubscriptions() {
      |     Subscription[] subscriptions = restTemplate.getForObject(
      | 	     subscriptionServiceURL + "api/all", Subscription[].class);
      |     return subscriptions;
      | }
      `----
      Test the endpoint
      ,----
      | ./bin/curl-subscription-all.sh
      `----

    - endpoint `/api/add/'

    - endpoint `/api/details/{id}'

    - endpoint `/api/cancel/{id}'


3.2.3 servicemysqldb
--------------------

  MySQL 8.0.23
  - ROOT password: Spring.1


3.2.4 serviceproxy
------------------

  Nginx 1.19


3.3 Using Security (8484)
~~~~~~~~~~~~~~~~~~~~~~~~~

  *servicepublicsec*
  Add Security dependencies in pom.xml
  ,----
  | <dependency>
  | 	<groupId>org.springframework.boot</groupId>
  | 	<artifactId>spring-boot-starter-security</artifactId>
  | </dependency>
  | <dependency>
  | 	<groupId>org.springframework.security</groupId>
  | 	<artifactId>spring-security-test</artifactId>
  | 	<scope>test</scope>
  | </dependency>
  `----


3.4 Alternative implementation: Using JPA and HATEOAS (8282)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  The Service *servicejpa* implements direct access to the H2 database
  publishing the JPA
  layer using the `@RepositoryRestResource' annotation.
  [https://spring.io/guides/gs/accessing-data-rest/]
  Use Spring Initializr to create a new Maven project with following
  dependencies:
  - Rest Repositories, Spring Data JPA, H2 Database
  Key entries in `pom.xml'
  ,----
  | <dependency>
  | 	<groupId>org.springframework.boot</groupId>
  | 	<artifactId>spring-boot-starter-data-rest</artifactId>
  | </dependency>
  `----
  - Repository Rest Resource annotation
    ,----
    | @RepositoryRestResource(collectionResourceRel = "subscription", path = "subscription")
    | public interface SubscriptionRepository extends PagingAndSortingRepository<Subscription, Long> {
    | 
    |     List<Subscription> findByFirstName(@Param("name") String name);
    | 
    | }
    `----
  - Get API info
    ,----
    | curl http://localhost:8282
    `----

    ,----
    | {
    |   "_links" : {
    |     "subscription" : {
    |       "href" : "http://localhost:8282/subscription{?page,size,sort}",
    |       "templated" : true
    |     },
    |     "profile" : {
    |       "href" : "http://localhost:8282/profile"
    |     }
    |   }
    | }
    `----

  - Get all
    ,----
    | ./bin/curl-jpa-all.sh
    | curl http://localhost:8282/subscription
    `----

    ,----
    | {
    |   "_embedded" : {
    |     "subscription" : [ {
    |       "email" : "default@email.com",
    |       "firstName" : "DefaultName",
    |       "gender" : "Female",
    |       "dateofbirthString" : "1991/01/21",
    |       "consentString" : true,
    |       "_links" : {
    | 	"self" : {
    | 	  "href" : "http://localhost:8282/subscription/4"
    | 	},
    | 	"subscription" : {
    | 	  "href" : "http://localhost:8282/subscription/4"
    | 	}
    |       }
    |     } ]
    |   },
    |   "_links" : {
    |     "self" : {
    |       "href" : "http://localhost:8282/subscription"
    |     },
    |     "profile" : {
    |       "href" : "http://localhost:8282/profile/subscription"
    |     },
    |     "search" : {
    |       "href" : "http://localhost:8282/subscription/search"
    |     }
    |   },
    |   "page" : {
    |     "size" : 20,
    |     "totalElements" : 1,
    |     "totalPages" : 1,
    |     "number" : 0
    |   }
    | }
    `----

  - Add
    ,----
    | ./bin/curl-jpa-add.sh "Name" "email@domain.com"
    | curl -i -H "Content-Type:application/json" \
    |      -d '{"email": "user01@domain.com", "firstName": "User01", "gender": "Male", "dateofbirthString": "1990/01/20"}' \
    |      http://localhost:8282/subscription
    `----

  - Details
    ,----
    | ./bin/curl-jpa-details.sh 4
    `----

    ,----
    | {
    |   "email" : "default@email.com",
    |   "firstName" : "DefaultName",
    |   "gender" : "Female",
    |   "dateofbirthString" : "1991/01/21",
    |   "consentString" : true,
    |   "_links" : {
    |     "self" : {
    |       "href" : "http://localhost:8282/subscription/4"
    |     },
    |     "subscription" : {
    |       "href" : "http://localhost:8282/subscription/4"
    |     }
    |   }
    | }
    `----

  - Delete
    ,----
    | ./bin/curl-jpa-delete.sh 4
    `----


4 Tasks
=======

  - Implement all the endpoints
  - Manage Exceptions
    [https://www.baeldung.com/exception-handling-for-rest-with-spring]
  - Document API
    [https://spring.io/guides/gs/testing-restdocs/]
    [https://swagger.io/docs/open-source-tools/swagger-editor/]
    [https://editor.swagger.io/]
    - Generate JSON with postman to import in swagger
      [https://swagger.io/tools/swagger-inspector/]
  - Implement the unit tests
    [https://spring.io/guides/gs/testing-web/]
  - Define CD/CI
    Jenkins file using BlueOcean plugin
  - Deploy in Kubernetes
    [https://spring.io/guides/gs/spring-boot-kubernetes/]
  - Use postman to analyse example UI requests
    [https://www.adidas.co.uk/on/demandware.store/Sites-adidas-GB-Site/en_GB/Newsletter-Subscribe?consent-level=2]


5 References
============

  [https://code.visualstudio.com/download]
  [https://code.visualstudio.com/docs/containers/overview]
  [https://spring.io/guides/tutorials/rest/]
  [https://spring.io/guides]
  [https://spring.io/guides/tutorials/rest/]
  [https://spring.io/projects/spring-boot]
  [https://spring.io/projects/spring-hateoas]
  [https://www.baeldung.com/spring-controller-vs-restcontroller]
  [https://www.nigmacode.com/java/crear-api-rest-con-spring/]
  [https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html]
  [https://www.baeldung.com/exception-handling-for-rest-with-spring]
  [https://stackoverflow.com/questions/29145370/how-can-i-initialize-a-mysql-database-with-schema-in-a-docker-container]
  [https://serverfault.com/questions/796762/creating-a-docker-mysql-container-with-a-prepared-database-scheme]
  [https://github.com/lindycoder/prepopulated-mysql-container-example]
  [https://hub.docker.com/_/mysql]
  [https://hub.docker.com/_/nginx]
  [http://nginx.org/en/docs/beginners_guide.html#conf_structure]
  [https://stackoverflow.com/questions/41766195/nginx-emerg-server-directive-is-not-allowed-here/41766811]
  [https://stackoverflow.com/questions/34865379/nginx-emerg-server-directive-is-not-allowed-here-in-etc-nginx-nginx-conf9]
  [https://www.domysee.com/blogposts/reverse-proxy-nginx-docker-compose]
  [https://www.nginx.com/resources/wiki/start/topics/examples/full/]
  [https://stackoverflow.com/questions/56879017/how-to-connect-frontend-to-backend-via-docker-compose-networks]
  [https://docs.docker.com/compose/networking/]
  [https://docs.docker.com/compose/startup-order/]
  [https://stackoverflow.com/questions/19333106/issue-with-parsing-the-content-from-json-file-with-jackson-message-jsonmappin]
  [https://stackoverflow.com/questions/49833093/swagger-running-with-local-json-file-in-docker-image]
  [https://github.com/swagger-api/swagger-ui/issues/5383]
  [https://hub.docker.com/r/swaggerapi/swagger-ui]
  [https://spin.atomicobject.com/2020/09/23/defined-docker-networks/]
  [https://wkrzywiec.medium.com/how-to-run-database-backend-and-frontend-in-a-single-click-with-docker-compose-4bcda66f6de]
  [https://stackoverflow.com/questions/56879017/how-to-connect-frontend-to-backend-via-docker-compose-networks]
  [https://stackoverflow.com/questions/31299098/how-can-i-generate-swagger-based-off-of-existing-postman-collection]
  [https://github.com/stoplightio/api-spec-converter]
  [https://hantsy.gitbook.io/build-a-restful-app-with-spring-mvc-and-angularjs/swagger]
  [https://swagger.io/docs/swagger-inspector/how-to-create-an-openapi-definition-using-swagger/]
  [https://swagger.io/resources/articles/documenting-apis-with-swagger/]
  [https://hantsy.gitbooks.io/build-a-restful-app-with-spring-mvc-and-angularjs/content/swagger.html]
