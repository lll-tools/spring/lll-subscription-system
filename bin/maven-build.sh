#!/bin/bash

## Colors
RED='\033[0;31m'
YELLOW='\033[1;33m'
GREEN='\033[0;32m'
NOCOLOR='\033[0m'

## Functions
log_exit () {
    if [ $1 -eq 0 ]; then
        echo -e "[${GREEN}OK${NOCOLOR}] $2"
    else
        if [ $1 -eq 2 ]; then
            echo -e "[${YELLOW}WARN${NOCOLOR}] $2"
        else
            echo -e "[${RED}ERR${NOCOLOR}] $2"
            exit 1
        fi
    fi
}

git pull --recurse-submodules
log_exit $? "Pull submodules ..."
git submodule update --remote
log_exit $? "Update submodules ..."
mvn --version
log_exit $? "Maven version ..."

export MYSQL_HOST=localhost
# export MYSQL_ROOT_PASSWORD=Spring.1
docker run --rm -d --name servicemysqldbuild \
       -p 3306:3306 \
       -e MYSQL_ROOT_PASSWORD=Spring.1 \
       -e MYSQL_DATABASE=spring_subscription \
       mysql:8.0.23
log_exit $? "Run MySQL 8.0.23 just for building packages ..."

docker exec -i servicemysqldbuild sh -c 'exec mysql -uroot -pSpring.1 -e "SHOW DATABASES"'
retval=$?
while [ $retval -ne 0 ]
do
    log_exit 2 "Waiting MySQL init ..."
    sleep 8
    docker exec -i servicemysqldbuild sh -c 'exec mysql -uroot -pSpring.1 -e "SHOW DATABASES"'
    retval=$?
done
log_exit $retval "Show databases ..."

cd servicejpamysql
mvn package
log_exit $? "MVN package servicejpamysql ..."
cd ..

cd servicepublicrest
mvn package
log_exit $? "MVN package serivepublicrest ..."
cd ..

cd servicejpa
mvn package
log_exit $? "MVN package servicejpa ..."
cd ..

docker stop servicemysqldbuild

# docker run -p 8585:8080 \
#        -e SWAGGER_JSON=/tmp/swagger.json \
#        -v `pwd`/doc:/tmp \
#        swaggerapi/swagger-ui:v3.21.0
