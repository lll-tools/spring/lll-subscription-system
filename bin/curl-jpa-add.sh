#!/bin/bash

NAME="$1"
[ -z "$NAME" ] && NAME="DefaultName"
EMAIL="$2"
[ -z "$EMAIL" ] && EMAIL="default@email.com"

JSON_STRING="{\"email\": \"$EMAIL\", \"firstName\": \"$NAME\", \"gender\": \"Female\", \"dateofbirthString\": \"1991/01/21\", \"consentString\": \"true\"}"
echo "JSON_STRING: ($JSON_STRING)"

curl -i -H "Content-Type:application/json" \
     -d "$JSON_STRING" \
     http://localhost:8282/subscription

# curl -X PUT -H "Content-Type:application/json" \
#      -d '{"firstName": "Name", "email": "name@domain.com"}' \
#      http://localhost:8080/subscripton/1

# curl -X PATCH -H "Content-Type:application/json" \
#      -d '{"firstName": "Name2"}' \
#      http://localhost:8080/subscription/1
