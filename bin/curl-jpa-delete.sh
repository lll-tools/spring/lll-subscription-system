#!/bin/bash

ID=$1
[ -z $ID ] && ID=1

curl -X DELETE http://localhost:8282/subscription/$ID
