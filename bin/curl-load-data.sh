#!/bin/bash

curl localhost:8181/api/add -d name=First -d email=someemail@someemailprovider.com

curl -i -H "Content-Type:application/json" \
     -d '{"email": "user01@domain.com", "firstName": "User01", "gender": "Female", "dateofbirthString": "1991/01/21"}, "consentString": "T"}' \
     http://localhost:8282/subscription

curl -i -H "Content-Type:application/json" \
     -d '{"email": "user02@domain.com", "firstName": "User02", "gender": "Male", "dateofbirthString": "1992/02/22"}, "consentString": "T"}' \
     http://localhost:8282/subscription

curl -i -H "Content-Type:application/json" \
     -d '{"email": "user03@domain.com", "firstName": "User03", "gender": "Female", "dateofbirthString": "1993/03/23"}, "consentString": "F"}' \
     http://localhost:8282/subscription


