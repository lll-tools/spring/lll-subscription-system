#!/bin/bash

NAME="$1"
[ -z "$NAME" ] && NAME="DefaultName"
EMAIL="$2"
[ -z "$EMAIL" ] && EMAIL="default@email.com"

curl http://localhost:8181/api/add -d name="$NAME" -d email="$EMAIL"
