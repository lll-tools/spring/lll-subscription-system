#!/bin/bash

#curl http://localhost:8080

echo -e "\n\nPUBLIC port (8080) ............................\n"
curl http://localhost:8080/api/subscriptions

echo -e "\n\nPROXY port (8383) .............................\n"
curl http://localhost:8383/api/all

echo -e "\n\nJPA-MYSQL port (8181) .........................\n"
curl http://localhost:8181/api/all

echo -e "\n\nJPA-H2 port (8282) ............................\n"
curl http://localhost:8282/subscription

echo -e "\n\nEnd ...........................................\n"
