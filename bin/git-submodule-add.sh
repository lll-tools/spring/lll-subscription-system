#/!bin/bash

#https://git-scm.com/book/en/v2/Git-Tools-Submodules

#git submodule add https://gitlab.com/lll-tools/spring/lll-subscription-servicejpa.git servicejpa
#git submodule add https://gitlab.com/lll-tools/spring/lll-subscription-servicejpamysql.git servicejpamysql
#git submodule add https://gitlab.com/lll-tools/spring/lll-subscription-servicepublicrest.git servicepublicrest

#git submodule add https://gitlab.com/lll-tools/spring/lll-subscription-servicemail.git servicemail

# git clone https://gitlab.com/lll-tools/spring/lll-subscription-servicejpa.git
# git clone https://gitlab.com/lll-tools/spring/lll-subscription-servicemail.git
# git clone https://gitlab.com/lll-tools/spring/lll-subscription-servicepublic.git

# git submodule init
# git submodule update --init --recursive
git submodule update --remote

# git push --recurse-submodules=check
# git push --recurse-submodules=on-demand
# git submodule foreach 'git stash'
